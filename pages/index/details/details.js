// pages/index/details/index.js
var WxParse = require('../../../wxParse/wxParse.js');

var app = getApp();

var activityId;

Page({
  data: {
    activityDetails: []
  },
  onLoad: function (option) {
    app.showToast();
    activityId = option.id;
  },
  onShow: function () {
    var page = this;
    wx.request({
      url: app.urlActiveDetails,
      data: {
        activityId: activityId
      },
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      // header: {}, // 设置请求的 header
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function (res) {
        // success
        var data = res.data.data;
        if (res.data.errorCode == 200) {
          if (data != null && data != "") {
            page.setData({ activityDetails: data.info });
            var article = data.info.description;
            WxParse.wxParse('article', 'html', article, page);
            wx.setNavigationBarTitle({
              title: data.info.title
            })

          }
        } else {

        }
      },
      fail: function () {
        // fail
      },
      complete: function () {
        app.hideToast();
      }
    })

  },
  onShareAppMessage: function () {
    return {
      title: '自定义分享标题',
      desc: '自定义分享描述',
      path: '/page/user?id=123'
    }
  }
})