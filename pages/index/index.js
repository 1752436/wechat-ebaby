//index.js
//获取应用实例

var app = getApp()

Page({
  data: {
    activityInfo: []
  },
  onLoad: function () {
    app.showToast();
  },
  onReady: function () {
    var page = this;
    wx.request({
      url: app.urlActiveLists,
      data: {},
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      // header: {}, // 设置请求的 header
      header: {
        'content-type': 'application/x-www-form-urlencoded' 
      },
      success: function (res) {
        // success
        var data = res.data.data;
        if (res.data.errorCode == 200) {
          if (data != null && data != "") {
            page.setData({ activityInfo: data.list });
          }
        } else {

        }
      },
      fail: function () {
        // fail
      },
      complete: function () {
        app.hideToast();
      }
    })

  },
  activityDetails: function (event) {
    wx.navigateTo({
      url: './details/details?id=' + event.currentTarget.dataset.activityid
    })
  }

})
