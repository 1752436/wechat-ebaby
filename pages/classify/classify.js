// pages/classify/classify.js
var app = getApp()

Page({
  data: {
    categoryList: []
  },
  onLoad: function () {
    app.showToast();
  },
  onReady: function () {
    var page = this;
    wx.request({
      url: app.urlAllCategoryStaffList,
      data: {},
      method: 'POST', // OPTIONS, GET, HEAD, POST, PUT, DELETE, TRACE, CONNECT
      // header: {}, // 设置请求的 header
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      success: function (res) {
        // success
        //console.log(app.toDecimal2((res.data.data.list[0].category_id)));
        var data = res.data.data;
        if (res.data.errorCode == 200) {
          if (data != null && data != "") {
            page.setData({ categoryList: data.list });
            // for(var i=0; i<data.list.length;i++){
            //   for(var j=0;j<data.list[i].second_list.length;j++){
            //     page.setData({ secondCategoryName: data.list[i].second_list[j].second_category_name});
            //   }
            // }
          }
        } else {

        }
      },
      fail: function () {
        // fail
      },
      complete: function () {
        app.hideToast();
      }
    })

  },
  activityDetails: function (event) {
    wx.navigateTo({
      url: './details/details?id=' + event.currentTarget.dataset.activityid
    })
  } 

})


