//app.js
var urlHead = "http://mybabygo.cn/index.php/api/";
App({
  //活动接口
  urlAllCategoryStaffList: urlHead + "category/staffCategoryList",
  urlActiveLists: urlHead + "activity/lists",
  urlActiveDetails: urlHead + "activity/info",
  //分类接口
  urlAllCategoryStaffList: urlHead + "category/staffCategoryList",
  showToast: function () {
    wx.showToast({
      title: '加载中',
      icon: 'loading',
      duration: 1000
    })
  },
  hideToast: function () {
    setTimeout(function () {
      wx.hideToast()
    }, 1000)
  },
  toDecimal2: function (x) {
    var result = "";
    if (x != null && x != "") {
      var f = parseFloat(x);
      if (isNaN(f)) {
        return false;
      }
      var f = Math.round(x * 100) / 100;
      var s = f.toString();
      var rs = s.indexOf('.');
      if (rs < 0) {
        rs = s.length;
        s += '.';
      }
      while (s.length <= rs + 2) {
        s += '0';
      }
      result = s;
    } else {
      result = "暂无"
    }
    return result;
  }
})